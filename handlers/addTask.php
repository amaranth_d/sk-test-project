<?php

include_once '../core/functions.php';
$errors = [];
if(!isset($_POST['title'])){
    $errors['title'] = 'Поле назви завдання пусте!';
}
if(!isset($_POST['status'])){
    $errors['status'] = 'Поле статусу завдання пусте!';
}
if(!isset($_POST['executors'])){
    $errors['executors'] = 'Мінімальна кількість виконавців: 1! Будь ласка, оберіть виконавців';
}

if(empty($errors) && !empty($_POST))
{
    $data = $_POST;
    $sql = 'INSERT INTO `tasks` (title, status, executors) VALUES ( "'. $data['title'] . '", "' . $data['status'] . '","' . $data['executors'] . '")';
    query($sql);
    header('Content-Type:application/json');
    echo json_encode($data);
}



