<?php

include_once '../core/functions.php';
$errors = [];

if(!isset($_POST['id'])){
    $errors['id'] = 'Ідентифікатор елемента не вказано';
}

if(empty($errors))
{
    $data = $_POST;
    $sql = 'DELETE FROM `tasks` WHERE id =' . $data['id'];
    query($sql);
    header('Content-Type:application/json, charset=UTF-8');
    echo json_encode($data);
}



