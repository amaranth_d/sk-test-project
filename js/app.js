$('#addTaskForm').submit( function(e)
{
    e.preventDefault();
    let data = $(this).serialize();
    $.ajax({
        type: 'POST',
        url: '/handlers/addTask.php',
        dataType: 'json',
        data: data,
        async: true,
        success: function (res)
        {
            $.ajax({
                url: '/templates/tasks_all.php',
                type: 'GET',
                data: '',
                success: function (resp)
                {
                    $('#addTaskForm').each(function () {this.reset()});
                    updateTableData(resp);
                }
            });
        }
    })
})

function removeRequest(id)
{
    $.ajax({
        type: 'POST',
        url: '/handlers/removeTask.php',
        data: 'id=' + id,
        async: true,
        success: function (res)
        {
            $.ajax({
                url: '/templates/tasks_all.php',
                type: 'GET',
                data: '',
                success: function (resp)
                {
                    updateTableData(resp);
                }
            });
        }
    })
}

function updateTableData(data)
{
    let form = $(`${data}`).filter('#tasks_table');
    $('#tasks_table').replaceWith(form);

}
