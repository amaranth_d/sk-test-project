<?php


CONST TEMPLATE_PATH = 'templates/';

function renderHtml(string $templateName = 'main', array $additionalVars = [], int $responseCode = 200)
{

    http_response_code($responseCode);

    extract($additionalVars);

    $template = TEMPLATE_PATH . $templateName . '.php';

    ob_start();
    include $template;
    $buffer = ob_get_contents();
    ob_end_clean();

    echo $buffer;
}

function query(string $sql, array $params = [])
{
    $settings = (require 'settings.php')['db'];
    try {
        $pdo = new PDO(
            'mysql:host=' . $settings['host'] .
            ';dbname=' . $settings['dbname'],
            $settings['user'],
            $settings['password']
        );
        $pdo->exec('SET NAMES UTF');
    } catch (PDOException $error)
    {
        renderHtml('500', ['error'=> $error]);
    }

    $query = $pdo->prepare($sql);
    $result = $query->execute($params);
    if(!$result) {
        return null;
    }
    return $query->fetchAll();
}