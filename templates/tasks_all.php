<?php
include_once 'header.php';
$tasks = query('SELECT * FROM `tasks`');
?>

<table border="2" id="tasks_table">
    <caption>Таблиця завдань</caption>
    <thead>
    <tr>
        <th>ID</th>
        <th>Замовлення</th>
        <th>ID Виконавців</th>
        <th>Статус</th>
        <th>Видалити</th>
    </tr>
    </thead>
    <tbody>
    <?php
    /** @var array $tasks */
    foreach ($tasks as $task): ?>
        <tr>
            <th>
                <?= $task['id']; ?>
            </th>
            <th>
                <?= $task['title']; ?>
            </th>
            <th><?= $task['executors'] ?></th>
            <th><?= $task['status'] ?></th>
            <th><button onclick="removeRequest(<?php echo $task['id']?>)"
                        type="button" class="button__remove">Видалити</button></th>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php include 'task_add.php' ?>
