<?php include 'header.php';
$executors = query('SELECT * FROM `executors`');
?>

<table border="2" id="tasks_table">
    <caption>Таблиця виконавців</caption>
    <thead>
    <tr>
        <th>ID</th>
        <th>Ім'я</th>
        <th>Фамілія</th>
        <th>ID Завдань</th>
    </tr>
    </thead>
    <tbody>
    <?php
    /** @var array $tasks */
    foreach ($executors as $executor): ?>
        <tr>
            <th>
                <?= $executor['id']; ?>
            </th>
            <th>
                <?= $executor['firstname']; ?>
            </th>
            <th><?= $executor['secondname']; ?></th>
            <th><?= $executor['tasks'] ?></th>
            </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php include 'footer.php'; ?>
