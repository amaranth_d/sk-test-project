<?php include_once 'header.php';
$executors = query('SELECT * FROM `executors`');
?>
<div class="form wrapper">
<form id="addTaskForm" action="handlers/addTask.php" class="tasks__add form">
    <label for="title">Назва нового завдання</label>
    <input id="title" type="text" name="title" placeholder="Введіть назву завдання">
    <label for="status">Статус</label>
    <select name="status" id="status">
        <option value="0" selected>Нова</option>
        <option value="1">Виконана</option>
        <option value="2">В роботі</option>
    </select>
    <label for="executors">
        Виконавці
    </label>
    <select required name="executors" id="executors" multiple>
        <?php foreach ($executors as $index => $ex): ?>
            <option value="<?= $ex['id'] ?>"><?= $ex['firstname']?></option>
        <?php endforeach; ?>
    </select>
    <button class="button" type="submit">Додати</button>
</form>
</div>
<?php include_once 'footer.php'; ?>
