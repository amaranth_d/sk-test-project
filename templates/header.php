<?php
include_once '../core/functions.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <title>Тестовое задание</title>
</head>
<body>
<header>
    <nav class="navigation">
        <a href="/templates/tasks_all.php" role="button" class="button">Усі завдання</a>
        <br>
        <a href="/templates/executors_all.php">Усі виконавці</a>
        <br>
        <a href="/templates/main.php">Головна</a>
        <hr>
    </nav>
</header>
